package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"encoding/json"
)

type Contact struct {
	Name  string `json:"nm" yaml:"Name"`
	Phone string `json:"ph" yaml:"Phone"`
}

func Index(w http.ResponseWriter, r *http.Request) {

	var data string
	data = "hello, I got your data"
	// data := "hello, I got your data"

	//data_bytes := []byte(data)
	//w.Write(data_bytes)

	_, err := w.Write([]byte(data))
	if err != nil {
		log.Printf("Error occurred: %v", err)
		return
	}
}

func GetAllContacts(w http.ResponseWriter, r *http.Request) {

	all_contacts := []Contact{
		Contact{
			Name: "John",
			Phone: "555-555-5555",
		},
		Contact{
			Name: "Spencer",
			Phone: "666-666-6666",
		},		
	}

	switch r.Method {
	case http.MethodGet:
		log.Printf("GET request received from %v", r.RemoteAddr)

		//data, err := json.MarshalIndent(all_contacts, "", "  ")
		//if err != nil {
		//	log.Printf("ERROR invalid request (serialization failed)")
		//	w.WriteHeader(http.StatusInternalServerError)
		//}
		//
		//// Return some meaningful data
		//_, err = w.Write(data)
		//if err!=nil {
		//	log.Printf("ERROR invalid request (write failed)")
		//	w.WriteHeader(http.StatusInternalServerError)
		//}

		err := json.NewEncoder(w).Encode(all_contacts)
		if err != nil {
			log.Printf("ERROR serialization failed")
		}
		
	default:
		log.Printf("ERROR invalid request (method not allowed)")
		http.Error(w, "Invalid request method", http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func main() {
	fmt.Println("Starting server...")

	r := mux.NewRouter()

	r.HandleFunc("/", Index)
	//r.HandleFunc("/contact/{id}", ContactById) // Come back to this
	r.HandleFunc("/contact/getAll", GetAllContacts)
	r.HandleFunc("/doc")

	log.Fatal(http.ListenAndServe(":8080", r))
}
